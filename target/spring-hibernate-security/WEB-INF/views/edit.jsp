<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registration</title>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=password], select {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #4CAF50;
                color: white;
                padding: 14px 20px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #45a049;
            }

            div {
                border-radius: 5px;
                background-color: #f2f2f2;
                padding: 20px;
            }
        </style>
    </head>
    <body>
        <div align="center">
            <form:form action="${pageContext.request.contextPath}/editCountry/${country.id}" method="post" modelAttribute="country">
                <table border="0">
                    <tr>
                        <td colspan="2" align="center"><h2>Edit Country</h2></td>
                    </tr>
                    <tr>
                        <td>Country Code:</td>
                        <td><form:input path="countryCode" value='${country.countryCode}'/></td>
                    </tr>
                    <tr>
                        <td>Country Name:</td>
                        <td><form:input path="countryName" value='${country.countryName}'/></td>
                    </tr>
                    <tr>
                    <br><td colspan="2" align="center"><input type="submit" value="Update" /></td>
                    </tr>
                </table>
            </form:form>
        </div>
    </body>
</html>
