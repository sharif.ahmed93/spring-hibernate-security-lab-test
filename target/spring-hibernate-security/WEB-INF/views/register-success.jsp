<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Registration Success</title>
<style>
#customers {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	border-collapse: collapse;
	width: 100%;
}

#customers td, #customers th {
	border: 1px solid #ddd;
	padding: 8px;
}

#customers tr:nth-child(even) {
	background-color: #f2f2f2;
}

#customers tr:hover {
	background-color: #ddd;
}

#customers th {
	padding-top: 12px;
	padding-bottom: 12px;
	text-align: left;
	background-color: #4CAF50;
	color: white;
}
</style>
</head>
<body>
	<table border="0">
		<tr>
			<td colspan="2" align="center"><h2>Spring MVC Form Demo -
					Registration Success</h2></td>
		</tr>
		<tr>
			<td>User Name:</td>
			<td>${user.username}</td>
		</tr>
		<tr>
			<td>E-mail:</td>
			<td>${user.email}</td>
		</tr>
		<tr>
			<td>Birthday (mm/dd/yyyy):</td>
			<td>${user.birthDate}</td>
		</tr>
		<tr>
			<td>Profession:</td>
			<td>${user.profession}</td>
		</tr>
		<tr>
			<td><a href="${pageContext.request.contextPath}/">Go Home</a></td>
		</tr>
	</table>

	<br>
	<h3>User List</h3>
	<c:if test="${!empty userList}">
		<table id="customers">
		<tr>
				<th>User Name</th>
				<th>E-mail</th>
				<th>Birthday (mm/dd/yyyy)</th>
				<th>Profession</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
			<c:forEach items="${userList}" var="user">
				<tr>
					<td>${user.username}</td>
					<td>${user.email}</td>
					<td>${user.birthDate}</td>
					<td>${user.profession}</td>
					<td><a href="<c:url value='/edit/${person.id}' />">Edit</a></td>
					<td><a href="<c:url value='/remove/${person.id}' />">Delete</a></td>
				</tr>
			</c:forEach>
	</table>
	</c:if>

	

</body>
</html>