package com.example.sharif.springmvc.services.team;

import com.example.sharif.springmvc.model.Team;

import java.util.List;

public interface TeamService {
    public void addTeam(Team team);

    public List<Team> getAllTeams();

    public void deleteTeam(int teamId);

    public Team updateTeam(Team team);

    public Team getTeam(int teamId);
}
