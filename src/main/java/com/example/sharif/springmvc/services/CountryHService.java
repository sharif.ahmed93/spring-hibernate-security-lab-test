package com.example.sharif.springmvc.services;

import com.example.sharif.springmvc.config.persistence.HibernateConfig;
import com.example.sharif.springmvc.exceptions.ResourceAlreadyExistsException;
import com.example.sharif.springmvc.exceptions.ResourceNotFoundException;
import com.example.sharif.springmvc.model.Country;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

@Service
public class CountryHService {

    private final HibernateConfig hibernateConfig;

    private static List<Country> countries = new ArrayList<Country>();

    private static final String[] COUNTRIES = { "BANGLADESH", "USA", "JAPAN", "NEPAL", "IRELAND", "GERMAN", "CANADA" };

    @Autowired
    public CountryHService(HibernateConfig hibernateConfig) {
        this.hibernateConfig = hibernateConfig;
    }

    private void addCountry(String countryName) {
        Country countryObj = new Country();
        countryObj.setId(countries.size() + 1);
        countryObj.setCountryName(countryName);
    }

    @Transactional
    public void addCountry(Country country) {
        Session session = hibernateConfig.getSession();
        Transaction transaction = session.beginTransaction();
        country.setId(countries.size() + 1);
        session.save(country);
        session.flush();
        transaction.commit();
    }

    public void checkCountryInList(Country c) {
        if (countries.stream().anyMatch(new Predicate<Country>() {
            @Override
            public boolean test(Country country) {
                return country.getId() == c.getId();
            }
        })) {
            throw new ResourceAlreadyExistsException("Country already exists in list");
        }
    }

    public Country getCountryByCode(String countryCode) {
        // **************************** HQL Start ******************************//
//		var session = hibernateConfig.getSession();
//		var transaction = session.beginTransaction();
//		var query = session
//				.getEntityManagerFactory()
//				.createEntityManager()
//				.createQuery("SELECT c from com.spring5.practice.model.Country c where c.countryCode=:countryCode", Country.class);
//		query.setParameter("countryCode", countryCode);
        // **************************** HQL End ******************************//

        // **************************** Criteria Query Start
        // **************************//
        CriteriaBuilder cb = hibernateConfig.getCriteriaBuilder();
        CriteriaQuery<Country> cq = cb.createQuery(Country.class);
        Root<Country> root = cq.from(Country.class);
        cq.where(cb.equal(root.get("countryCode"), countryCode));
        List<Country> result = hibernateConfig.getSession().getEntityManagerFactory().createEntityManager().createQuery(cq).getResultList();

        // **************************** Criteria Query End **************************//
        return Optional.ofNullable(result.get(0)).orElseThrow(() -> new ResourceNotFoundException("Country not found with this code"));
    }

    public List<Country> getAll() {

        // **************************** HQL Start ******************************//
//		 var session = hibernateConfig.getSession(); session.beginTransaction(); var
//		 query = session .getEntityManagerFactory() .createEntityManager().createQuery("SELECT c from com.spring5.practice.model.Country c ",
//		 Country.class);
//		 return query.getResultList();
        // **************************** HQL End ******************************//

        // **************************** Criteria Query Start
        // **************************//
        CriteriaBuilder cb = hibernateConfig.getCriteriaBuilder();
        CriteriaQuery<Country> cq = cb.createQuery(Country.class);
        Root<Country> root = cq.from(Country.class);
        cq.select(root);
        List<Country> countries = hibernateConfig.getSession().getEntityManagerFactory().createEntityManager().createQuery(cq).getResultList();
        // **************************** Criteria Query End **************************//
        return countries;
    }
}
