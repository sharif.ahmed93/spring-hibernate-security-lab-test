package com.example.sharif.springmvc.services.player;

import com.example.sharif.springmvc.dao.player.PlayerDao;
import com.example.sharif.springmvc.model.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PlayerServiceImpl implements PlayerService {

    @Autowired
    PlayerDao playerDao;

    @Override
    @Transactional
    public void addPlayer(Player player) {
        playerDao.addPlayer(player);
    }

    @Override
    @Transactional
    public List<Player> getAllPlayers() {
        return playerDao.getAllPlayers();
    }

    @Override
    @Transactional
    public void deletePlayer(int playerId) {
        playerDao.deletePlayer(playerId);
    }

    @Override
    @Transactional
    public Player updatePlayer(Player player) {
        return playerDao.updatePlayer(player);
    }

    @Override
    @Transactional
    public Player getPlayer(int playerId) {
        return playerDao.getPlayer(playerId);
    }
}
