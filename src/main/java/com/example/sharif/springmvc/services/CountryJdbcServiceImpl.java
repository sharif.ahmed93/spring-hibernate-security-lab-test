package com.example.sharif.springmvc.services;

import com.example.sharif.springmvc.dao.CountryDao;
import com.example.sharif.springmvc.dao.CountryJdbcDaoImpl;
import com.example.sharif.springmvc.model.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryJdbcServiceImpl implements CountryService {

    @Autowired
    CountryJdbcDaoImpl countryDao;

    public void checkCountryInList(Country country) {
        countryDao.checkCounrtyInList(country);
    }

    public void addCountry(Country country) {
        countryDao.addCountry(country);
    }

    public void deleteCountryByID(long id) {
        countryDao.deleteCountryByID(id);
    }

    public void updateCountryByID(long id, Country country) {
        countryDao.updateCountryByID(id, country);
    }

    public Country getCountryByCode(long id) {
        return countryDao.getCountryByCode(id);
    }

    public List<Country> getAll() {
        return countryDao.getAll();
    }

}
