package com.example.sharif.springmvc.services.team;

import com.example.sharif.springmvc.dao.team.TeamDao;
import com.example.sharif.springmvc.model.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TeamServiceImpl implements TeamService {

    @Autowired
    TeamDao teamDao;

    @Override
    @Transactional
    public void addTeam(Team team) {
        teamDao.addTeam(team);
    }

    @Override
    @Transactional
    public List<Team> getAllTeams() {
        return teamDao.getAllTeams();
    }

    @Override
    @Transactional
    public void deleteTeam(int teamId) {
        teamDao.deleteTeam(teamId);
    }

    @Override
    @Transactional
    public Team updateTeam(Team team) {
        return teamDao.updateTeam(team);
    }

    @Override
    @Transactional
    public Team getTeam(int teamId) {
        return teamDao.getTeam(teamId);
    }

    public void setTeamDao(TeamDao teamDao) {
        this.teamDao = teamDao;
    }
}
