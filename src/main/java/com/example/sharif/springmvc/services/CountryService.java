/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.sharif.springmvc.services;


import com.example.sharif.springmvc.model.Country;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * @author R041802050
 */


public interface CountryService {

    public void addCountry(Country country);

    public void deleteCountryByID(long id);

    public void checkCountryInList(Country country);

    public void updateCountryByID(long id, Country country);

    public Country getCountryByCode(long id);

    public List<Country> getAll();
}
