package com.example.sharif.springmvc.controllers.player;

import com.example.sharif.springmvc.model.Player;
import com.example.sharif.springmvc.model.Team;
import com.example.sharif.springmvc.services.player.PlayerService;
import com.example.sharif.springmvc.services.team.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@Controller
public class PlayerController {

    @Autowired
    private PlayerService playerService;

    @GetMapping(value = "/")
    public ModelAndView listEmployee(ModelAndView model) throws IOException {
        List<Player> listOfPlayer = playerService.getAllPlayers();
        model.addObject("listOfPlayer", listOfPlayer);
        model.setViewName("home");
        return model;
    }

    @GetMapping(value = "/newEmployee")
    public ModelAndView newContact(ModelAndView model) {
        Player player = new Player();
        model.addObject("employee", player);
        model.setViewName("EmployeeForm");
        return model;
    }

    @PostMapping(value = "/saveEmployee")
    public ModelAndView saveEmployee(@ModelAttribute Player player) {
        if (player.getId() == 0) { // if employee id is 0 then creating the
            // employee other updating the employee
            playerService.addPlayer(player);
        } else {
            playerService.updatePlayer(player);
        }
        return new ModelAndView("redirect:/");
    }

    @GetMapping(value = "/deleteEmployee")
    public ModelAndView deleteEmployee(HttpServletRequest request) {
        int playerId = Integer.parseInt(request.getParameter("id"));
        playerService.deletePlayer(playerId);
        return new ModelAndView("redirect:/");
    }

    @GetMapping(value = "/editEmployee")
    public ModelAndView editContact(HttpServletRequest request) {
        int playerId = Integer.parseInt(request.getParameter("id"));
        Player player = playerService.getPlayer(playerId);
        ModelAndView model = new ModelAndView("EmployeeForm");
        model.addObject("employee", player);

        return model;
    }

}

