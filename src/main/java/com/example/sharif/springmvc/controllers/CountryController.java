package com.example.sharif.springmvc.controllers;


import com.example.sharif.springmvc.model.Country;
import com.example.sharif.springmvc.services.CountryHService;
import com.example.sharif.springmvc.services.CountryJdbcServiceImpl;
import com.example.sharif.springmvc.services.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class CountryController {

	@Autowired
	private CountryJdbcServiceImpl countryService;

	/*@GetMapping("/country/add")
	public String addCountry_GET(Model model) {
		model.addAttribute("pageTitle", "Add Country");
		model.addAttribute("country", new Country());
		model.addAttribute("message", "Please add a country");
		return "country/add";
	}

	@PostMapping("/country/add")
	public String addCountry(Model model, @ModelAttribute(name = "country") Country country) {
		countryService.addCountry(country);
		model.addAttribute("message", "Country added successfully");
		return "redirect:/country/show-all";
	}

	@GetMapping("/country/show-all")
	public String showAll_GET(Model model) {
		model.addAttribute("pageTitle", "Country List");
		model.addAttribute("countries", countryService.getAll());
		model.addAttribute("message", "Showing all countries");
		return "country/show-all";
	}

	@GetMapping("/country/show/{code}")
	public String showAll_GET(@PathVariable(name = "code") String code, Model model) {
		model.addAttribute("country", countryService.getCountryByCode(code));
		model.addAttribute("message", "Showing country with code");
		return "country/show-by-code";
	}*/

	@GetMapping("/addCountry")
	public String viewAddCountryPage(Model model) {
		Country country = new Country();
		model.addAttribute("country", country);
		return "add-country";
	}

	@PostMapping("/addCountry")
	public String addCountry(Model model, @ModelAttribute("country") Country country) {
		countryService.addCountry(country);
		model.addAttribute("message", "Country added successfully");
		return "redirect:/showAllCountries"; //this redirect the controller to /showAllCountries url mapping and call the method showAll_GET()
	}

	@GetMapping("/editCountry/{id}")
	public String showEditCountry(Model model, @PathVariable("id") String id) {
		model.addAttribute("country", countryService.getCountryByCode(Long.parseLong(id)));
		return "edit";
	}

	@PostMapping("/editCountry/{id}")
	public String editCountry(Model model, @ModelAttribute("country") Country country, @PathVariable("id") String id) {
		countryService.updateCountryByID(Long.parseLong(id), country);
		model.addAttribute("message", "Country update successfully");
		return "redirect:/showAllCountries";
	}

	@GetMapping("/deleteCountry/{id}")
	public String deleteCountry(Model model, @PathVariable("id") String id) {
		countryService.deleteCountryByID(Long.parseLong(id));
		model.addAttribute("message", "Country deleted successfully");
		return "redirect:/showAllCountries";
	}

	@GetMapping("/showAllCountries")
	public String showAll_GET(Model model) {
		model.addAttribute("countryList", countryService.getAll());
		model.addAttribute("message", "Showing all countries");
		return "show-all-country";
	}

}
