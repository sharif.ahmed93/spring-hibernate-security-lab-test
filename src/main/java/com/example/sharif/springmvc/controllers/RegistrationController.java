package com.example.sharif.springmvc.controllers;


import com.example.sharif.springmvc.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Controller
public class RegistrationController {

	@GetMapping("/register")
	public String viewRegistration(Model model) {
		User userForm = new User();
		model.addAttribute("userForm", userForm);

		List<String> professionList = new ArrayList<>();
		professionList.add("Developer");
		professionList.add("Designer");
		professionList.add("IT Manager");
		model.addAttribute("professionList", professionList);

		return "register";
	}

	@PostMapping("/register")
	public String doRegistration(@ModelAttribute("userForm") User user, Model model) {

		List<User>userList = new ArrayList<User>();
		Random rndm = new Random(); 
		user.setUserId(rndm.toString());
		userList.add(user);
		// implement your own registration logic here...

		// for testing purpose:
		System.out.println("userid: " + user.getUserId());
		System.out.println("username: " + user.getUsername());
		System.out.println("password: " + user.getPassword());
		System.out.println("email: " + user.getEmail());
		System.out.println("birth date: " + user.getBirthDate());
		System.out.println("profession: " + user.getProfession());

		model.addAttribute("user", user);
		model.addAttribute("userList", userList);
		return "register-success";
	}

}
