package com.example.sharif.springmvc.controllers.team;

import com.example.sharif.springmvc.model.Team;
import com.example.sharif.springmvc.services.team.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@Controller
public class TeamController {

    @Autowired
    private TeamService teamService;

    @GetMapping(value = "/")
    public ModelAndView listEmployee(ModelAndView model) throws IOException {
        List<Team> listOfTeam = teamService.getAllTeams();
        model.addObject("listOfTeam", listOfTeam);
        model.setViewName("home");
        return model;
    }

    @GetMapping(value = "/newEmployee")
    public ModelAndView newContact(ModelAndView model) {
        Team employee = new Team();
        model.addObject("employee", employee);
        model.setViewName("EmployeeForm");
        return model;
    }

    @PostMapping(value = "/saveEmployee")
    public ModelAndView saveEmployee(@ModelAttribute Team employee) {
        if (employee.getId() == 0) { // if employee id is 0 then creating the
            // employee other updating the employee
            teamService.addTeam(employee);
        } else {
            teamService.updateTeam(employee);
        }
        return new ModelAndView("redirect:/");
    }

    @GetMapping(value = "/deleteEmployee")
    public ModelAndView deleteEmployee(HttpServletRequest request) {
        int employeeId = Integer.parseInt(request.getParameter("id"));
        teamService.deleteTeam(employeeId);
        return new ModelAndView("redirect:/");
    }

    @GetMapping(value = "/editEmployee")
    public ModelAndView editContact(HttpServletRequest request) {
        int employeeId = Integer.parseInt(request.getParameter("id"));
        Team employee = teamService.getTeam(employeeId);
        ModelAndView model = new ModelAndView("EmployeeForm");
        model.addObject("employee", employee);

        return model;
    }
}
