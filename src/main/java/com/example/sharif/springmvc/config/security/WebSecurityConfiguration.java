package com.example.sharif.springmvc.config.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser("admin").password("1234").roles("ADMIN");
        auth.inMemoryAuthentication().withUser("user").password("1234").roles("USER");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http
            .authorizeRequests()
            .antMatchers("/images/**", "/css/**", "/js/**").permitAll()
        .and()
                .authorizeRequests()
                .antMatchers("/addCountry").hasRole("ADMIN")
                .antMatchers("/deleteCountry/{id}").hasRole("ADMIN")
                .antMatchers("/showAllCountries").hasAnyRole("ADMIN","USER")
                .antMatchers("/editCountry/{id}").hasAnyRole("USER")
                .anyRequest().authenticated()
        .and()
                .formLogin()
                .loginPage("/login").loginProcessingUrl("/login-processing")
                .permitAll()
                .usernameParameter("username")
                .usernameParameter("password")
                .defaultSuccessUrl("/")
                .failureUrl("/login?error=true")
        .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/");
    }
}
