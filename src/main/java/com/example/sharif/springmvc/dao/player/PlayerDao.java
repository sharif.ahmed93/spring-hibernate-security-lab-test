package com.example.sharif.springmvc.dao.player;

import com.example.sharif.springmvc.model.Player;
import com.example.sharif.springmvc.model.Team;

import java.util.List;

public interface PlayerDao {
    public void addPlayer(Player player);

    public List<Player> getAllPlayers();

    public void deletePlayer(int playerId);

    public Player updatePlayer(Player player);

    public Player getPlayer(int playerId);
}
