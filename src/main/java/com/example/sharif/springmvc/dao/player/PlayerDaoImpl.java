package com.example.sharif.springmvc.dao.player;

import com.example.sharif.springmvc.model.Player;
import com.example.sharif.springmvc.model.Team;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class PlayerDaoImpl implements PlayerDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public void addPlayer(Player player) {
        sessionFactory.getCurrentSession().saveOrUpdate(player);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Player> getAllPlayers() {
        return sessionFactory.getCurrentSession().createQuery("from Player ").list();
    }

    @Override
    public void deletePlayer(int playerId) {
        Player player = (Player) sessionFactory.getCurrentSession().load(Player.class, playerId);
        if (null != player) {
            this.sessionFactory.getCurrentSession().delete(player);
        }
    }

    @Override
    public Player updatePlayer(Player player) {
        sessionFactory.getCurrentSession().update(player);
        return player;
    }

    @Override
    public Player getPlayer(int playerId) {
        return (Player) sessionFactory.getCurrentSession().get(Player.class, playerId);
    }
}
