/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.sharif.springmvc.dao;


import com.example.sharif.springmvc.model.Country;

import java.util.List;

public interface CountryDao {

    public void addCountry(Country country);

    public void deleteCountryByID(long id);

    public void checkCounrtyInList(Country country);

    public void updateCountryByID(long id, Country country);

    public Country getCountryByCode(long id);

    public List<Country> getAll();

}
