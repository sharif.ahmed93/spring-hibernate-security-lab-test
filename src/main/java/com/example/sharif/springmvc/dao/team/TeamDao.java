package com.example.sharif.springmvc.dao.team;

import com.example.sharif.springmvc.model.Team;

import java.util.List;

public interface TeamDao {
    public void addTeam(Team team);

    public List<Team> getAllTeams();

    public void deleteTeam(int teamId);

    public Team updateTeam(Team team);

    public Team getTeam(int teamId);
}
