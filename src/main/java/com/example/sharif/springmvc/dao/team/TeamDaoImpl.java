package com.example.sharif.springmvc.dao.team;

import com.example.sharif.springmvc.model.Team;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TeamDaoImpl implements TeamDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addTeam(Team team) {
        sessionFactory.getCurrentSession().saveOrUpdate(team);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Team> getAllTeams() {
        return sessionFactory.getCurrentSession().createQuery("from Team").list();
    }

    @Override
    public void deleteTeam(int teamId) {
        Team team = (Team) sessionFactory.getCurrentSession().load(Team.class, teamId);
        if (null != team) {
            this.sessionFactory.getCurrentSession().delete(team);
        }
    }

    @Override
    public Team updateTeam(Team team) {
        sessionFactory.getCurrentSession().update(team);
        return team;
    }

    @Override
    public Team getTeam(int teamId) {
        return (Team) sessionFactory.getCurrentSession().get(Team.class, teamId);
    }
}
