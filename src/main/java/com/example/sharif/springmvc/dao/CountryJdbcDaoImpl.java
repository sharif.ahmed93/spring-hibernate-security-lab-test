package com.example.sharif.springmvc.dao;


import com.example.sharif.springmvc.exceptions.ResourceAlreadyExistsException;
import com.example.sharif.springmvc.model.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class CountryJdbcDaoImpl implements CountryDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public void CountryJdbcDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        String queryString = "create table if not exists country(" + "id bigint not null auto_increment PRIMARY KEY, "
                + "country_code varchar(20), " + "country_name varchar(100)" + "); ";
        this.jdbcTemplate.execute(queryString);
    }

    @Override
    public void addCountry(Country country) {
        //checkCounrtyInList(country);
        String queryString = "insert into country(country_code, country_name) values(?, ?); ";
        int resultSet = jdbcTemplate.update(queryString, country.getCountryCode(), country.getCountryName());
        if (resultSet < 1) {
            throw new RuntimeException("Failed to inserted country.");
        }
    }

    @Override
    public void deleteCountryByID(long id) {
        //checkCounrtyInList(country);
        String queryString = "delete from country where id=?";
        //jdbcTemplate.update(queryString, id);
        int resultSet = jdbcTemplate.update(queryString, id);
        if (resultSet < 1) {
            throw new RuntimeException("Failed to deleted country.");
        }
    }

    @Override
    public void checkCounrtyInList(Country country) {
        String queryString = "select count(*) as count from country c where c.country_code=?";
        Map<String, Object> map = jdbcTemplate.queryForMap(queryString, country.getCountryCode());
        if (Integer.parseInt(map.get("count").toString()) > 0) {
            throw new ResourceAlreadyExistsException("Country with this Country code is already in DB.");
        }
    }

    @Override
    public void updateCountryByID(long id, Country country) {
        //checkCounrtyInList(country);
        String queryString = "update country set country_code=?,country_name=? where id=?";
        //jdbcTemplate.update(queryString, country.getCountryCode(),country.getCountryName(),id);
        int resultSet = jdbcTemplate.update(queryString, country.getCountryCode(), country.getCountryName(), id);
        if (resultSet < 1) {
            throw new RuntimeException("Failed to update country.");
        }
    }

    @Override
    public Country getCountryByCode(long id) {
        String queryString = "select id, country_code, country_name from country where id=?";
        ArrayList<Country> countries = new ArrayList<Country>();
        Map countryMap = jdbcTemplate.queryForMap(queryString, id);
        Country country = new Country();
        country.setId(Long.parseLong(countryMap.get("id").toString()));
        country.setCountryCode(countryMap.get("country_code").toString());
        country.setCountryName(countryMap.get("country_name").toString());
        return country;
    }

    @Override
    public List<Country> getAll() {
        String queryString = "select id, country_code, country_name from country";
        ArrayList<Country> countries = new ArrayList<Country>();
        jdbcTemplate.queryForList(queryString).forEach(countryMap -> {
            Country country = new Country();
            country.setId(Long.parseLong(countryMap.get("id").toString()));
            country.setCountryCode(countryMap.get("country_code").toString());
            country.setCountryName(countryMap.get("country_name").toString());
            countries.add(country);
        });

        return countries;
    }

}
