package com.example.sharif.springmvc.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Player {
    @Id
    private int id;

    public int getId() {
        return id;
    }
}
