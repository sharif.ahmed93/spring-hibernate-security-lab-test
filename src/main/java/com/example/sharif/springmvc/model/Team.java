package com.example.sharif.springmvc.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;
@Entity
public class Team implements Serializable {
    @Id
    private long id;
    private String teamName;
    @OneToOne
    private Country country;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
