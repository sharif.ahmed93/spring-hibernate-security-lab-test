<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="ISO-8859-1">
        <title>Registration Success</title>
        <style>
            #customers {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            #customers td, #customers th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            #customers tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            #customers tr:hover {
                background-color: #ddd;
            }
            #customers a.home{
                text-decoration: none;
                text-align: center
            }
            #customers th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #4CAF50;
                color: white;
            }
        </style>
    </head>
    <body>
        <h3>${message}</h3>
        <table id="customers">
            <tr>
                <th>Country Code</th>
                <th>Country Name</th>
                <th>Action</th>
            </tr>
            <c:forEach items="${countryList}" var="country">
                <tr>
                    <td>${country.countryCode}</td>
                    <td>${country.countryName}</td>
                    <td><a href="<c:url value='${pageContext.request.contextPath}/editCountry/${country.id}'/>">Edit</a>/<a href="<c:url value='${pageContext.request.contextPath}/deleteCountry/${country.id}'/>">Delete</a></td>
                </tr>
            </c:forEach>
            <tr>
                <td><a class="home" href="<c:url value='${pageContext.request.contextPath}/'/>">Go Home</a></td>
            </tr>
        </table>
    </body>
</html>