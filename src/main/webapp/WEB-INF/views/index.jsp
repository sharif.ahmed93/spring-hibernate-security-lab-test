<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Home</title>
    <style>
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 35%;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        #customers tr:hover {
            background-color: #ddd;
        }

        #customers tr a{
            text-decoration: none;text-align: center
        }

        #customers tr td {
            text-align: center
        }

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }
    </style>
</head>
<body>
<div align="center">
    <form:form>
        <table border="0" id="customers">
            <tr>
                <td colspan="2" align="center"><h1>Welcome to ICC</h1></td>
            </tr>
            <tr>
                <td><a href="${pageContext.request.contextPath}/register">Register</a></td>
            </tr>
            <tr>
                <td><a href="${pageContext.request.contextPath}/addCountry">Add Country</a></td>
            </tr>
            <tr>
                <td><a href="${pageContext.request.contextPath}/showAllCountries">Show All Country</a></td>
            </tr>
        </table>
    </form:form>
</div>
</body>
</html>
